# How to Stay Protected on Public Wi-Fi #

Getting free internet is never a bad thing and actually pretty awesome. The problem is, that public Wi-Fi offers nothing close to the security that your home Wi-Fi does. This is because it is open for everybody and unsecured.
What this means is that it is easy for hackers to sniff out your device and connection using software. They can intercept data packets and thus learn some valuable and sensitive information that you would rather they didn’t. 

Here are two tips on how to stay protected on public Wi-Fi:

### Expose no Personal Details on Public Wi-Fi ###

As mentioned above, hackers can intercept data sent to and from your device when using public Wi-Fi. For that reason, never use these networks to access online bank accounts, email accounts, e-wallet accounts and anything else that could be valuable to a hacker. Just typing in a username and password could give them everything that they need.

### Use a VPN at all Times When Using Public Wi-Fi ###

Probably the best tip is to use a virtual private network (VPN) when connected to a public Wi-Fi hotspot. This effective software will connect you to an encrypted network of computers that hides your IP address. As they encrypt your data, hackers cannot intercept it. A VPN does not cost much and sometimes you can even get them for free.

### Summing Up ###

If you can take heed of the two tips above, your online activity and your personal information will be so much safer when using public Wi-Fi.  At the very least, get yourself a reliable VPN to use.

[https://internetetsecurite.ch](https://internetetsecurite.ch)